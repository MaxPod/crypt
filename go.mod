module gitlab.com/MaxPod/crypt

go 1.15

require (
	cloud.google.com/go/firestore v1.3.0
	github.com/coreos/etcd v3.3.25+incompatible
	github.com/coreos/go-semver v0.3.0 // indirect
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	gitlab.com/MaxPod/consul_api v1.0.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	google.golang.org/api v0.33.0
	google.golang.org/grpc v1.33.1
)
